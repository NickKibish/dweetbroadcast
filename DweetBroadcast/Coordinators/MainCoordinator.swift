//
//  MainCoordinator.swift
//  DweetBroadcast
//
//  Created by Nick Kibish on 3/7/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

/// Coordinator which set root view controller
class MainCoordinator {
    private let window: UIWindow?
    private let disposeBag = DisposeBag()
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    func displayDataChart() {
        let vc = UIStoryboard.main.instantiateViewController(DataChartViewController.self)
        vc.coordinator = self 
        let vm = DataChartViewModel()
        vc.viewModel = vm
        let nc = UINavigationController(rootViewController: vc)
        self.window?.rootViewController = nc
    }
    
    func displayTutorialIfNeeded() {
        let tutorialKey = "TutorialDisplayed"
        guard UserDefaults.standard.bool(forKey: tutorialKey) != true else {
            return
        }
        
        UserDefaults.standard.set(true, forKey: tutorialKey)
        
        let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "TutorialViewController")
        vc.modalPresentationStyle = .overCurrentContext
        self.window?.rootViewController?.present(vc, animated: false, completion: { [weak vc] in
            let tap = UITapGestureRecognizer()
            tap.numberOfTapsRequired = 2
            tap.rx.event.subscribe(
                onNext: { _ in
                    vc?.dismiss(animated: true, completion: nil)
                }
            )
            .disposed(by: self.disposeBag)
            vc?.view.addGestureRecognizer(tap)
        })
    }
}
