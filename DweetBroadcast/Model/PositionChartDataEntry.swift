//
//  PositionChartDataEntry.swift
//  DweetBroadcast
//
//  Created by Nick Kibish on 3/6/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import Foundation
import Charts
import DweetNetworkService

/// Altitude chart data model
class PositionChartDataEntry: ChartDataEntry {
    init?(dweet: Dweet<Temperature>) {
        guard let created = Date(isoString: dweet.created)?.timeIntervalSince1970 else { return nil }
        super.init(x: created, y: dweet.content.accelerometerData.z)
    }
    
    required init() {
        fatalError("init() has not been implemented")
    }
}
