//
//  Date+Ext.swift
//  DweetBroadcast
//
//  Created by Nick Kibish on 3/6/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import Foundation

extension Date {
    /// Init with ISO string
    ///
    /// - Parameter isoString: date string from server
    init?(isoString: String) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        guard let date = formatter.date(from: isoString) else { return nil }
        
        self.init(timeIntervalSince1970: date.timeIntervalSince1970)
    }
}
