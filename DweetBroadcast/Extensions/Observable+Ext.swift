//
//  Observable+Ext.swift
//  DweetBroadcast
//
//  Created by Nick Kibish on 3/6/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import Foundation
import RxSwift

public protocol Optionable {
    associatedtype Wrapped
    var value: Wrapped? { get }
}

extension Optional: Optionable {
    public var value: Wrapped? {
        return self
    }
}

extension ObservableType where E: Optionable {
    /// Unwrap observable's element.
    ///
    /// - Returns: Value or empty observable
    func unwrap() -> Observable<E.Wrapped> {
        return self.flatMap { element -> Observable<E.Wrapped> in
            guard let value = element.value else {
                return Observable<E.Wrapped>.empty()
            }
            return Observable<E.Wrapped>.just(value)
        }
    }
}
