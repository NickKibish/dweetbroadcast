//
//  UIStoryboard+Ext.swift
//  DweetBroadcast
//
//  Created by Nick Kibish on 3/7/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import UIKit

extension UIStoryboard {
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    /// Create instance of view controller by provided type
    ///
    /// - Parameter vcClass: viewController's class
    /// - Returns: view controller instance 
    func instantiateViewController<T:UIViewController>(_ vcClass: T.Type) -> T {
        let id = String(describing: vcClass)
        return self.instantiateViewController(withIdentifier: id) as! T
    }
}
