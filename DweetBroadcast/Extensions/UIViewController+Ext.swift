//
//  UIViewController+Ext.swift
//  DweetBroadcast
//
//  Created by Nick Kibish on 3/7/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import UIKit

/// Unified interface for setting application's appearance
protocol Appearance {
    func setColorScheme(_ colorScheme: ApplicationColorScheme)
}

extension Appearance where Self: UIViewController {
    func setColorScheme(_ colorScheme: ApplicationColorScheme) {
        self.view.backgroundColor = colorScheme.mainColor
        self.navigationController?.navigationBar.barTintColor = colorScheme.secondaryColor
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor:colorScheme.textColor1]
    }
}

