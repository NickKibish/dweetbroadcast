//
//  AxisDateValueFormatter.swift
//  DweetBroadcast
//
//  Created by Nick Kibish on 3/7/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import Foundation
import Charts

/// Transform time interval into minutes and seconds
class DatesAxisFormatter: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let date = Date(timeIntervalSince1970: value)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "mm:ss"
        return dateFormatter.string(from: date)
    }
}
