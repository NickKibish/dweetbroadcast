//
//  LoopIterator.swift
//  DweetBroadcast
//
//  Created by Nick Kibish on 3/7/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import Foundation

/// Iterate through the collection. If call `next` on last element it returnes the first one.
struct LoopIterator<Base: Collection>: IteratorProtocol {
    
    private let collection: Base
    private var index: Base.Index
    
    init(collection: Base) {
        self.collection = collection
        self.index = collection.startIndex
    }
    
    mutating func next() -> Base.Iterator.Element? {
        guard !collection.isEmpty else { return nil }
        guard collection.count > 1 else { return collection.first }
        
        let result = collection[index]
        collection.formIndex(after: &index)
        if index == collection.endIndex {
            index = collection.startIndex
        }
        return result
    }
    
    mutating func skip(_ count: Int) {
        guard count > 0 else { return }
        _ = self.next()
        self.skip(count - 1)
    }
    
}
