//
//  AppDelegate.swift
//  DweetBroadcast
//
//  Created by Nick Kibish on 3/3/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var coordinator: MainCoordinator!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.coordinator = MainCoordinator(window: self.window)
        self.coordinator.displayDataChart()
        
        return true
    }


}

