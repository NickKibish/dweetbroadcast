//
//  ColorScheme.swift
//  DweetBroadcast
//
//  Created by Nick Kibish on 3/4/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift

/// Interface for color scheme. 
protocol ApplicationColorScheme {
    var mainColor: UIColor { get }
    var secondaryColor: UIColor { get }
    
    var dataColor1: UIColor { get }
    var dataColor2: UIColor { get }
    
    var textColor1: UIColor { get }
    var textColor2: UIColor { get }
}

struct MainAppColorScheme: ApplicationColorScheme {
    let mainColor = UIColor(hex6: 0x009ADA)
    let secondaryColor = UIColor(hex6: 0x0080B7)
    let dataColor1 = UIColor(hex6: 0xDC582A)
    let dataColor2 = UIColor(hex6: 0xBED62F)
    let textColor1 = UIColor.white
    let textColor2 = UIColor(hex6: 0x888888)
}
