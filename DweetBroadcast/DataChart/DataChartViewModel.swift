//
//  DataChartViewModel.swift
//  DweetBroadcast
//
//  Created by Nick Kibish on 3/6/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import RxCocoa
import RxSwift
import DweetNetworkService
import Charts

class DataChartViewModel {
    private let disposeBag = DisposeBag()
    private let dataService: DweetService<Temperature>
    private let newDweet = PublishSubject<Double>()
    private let colorSet: ApplicationColorScheme
    
    private lazy var temprData: LineChartData = self.createData(label: "Temperature", lineColor: self.colorSet.dataColor1)
    private lazy var altitudeData: LineChartData = self.createData(label: "Altitude", lineColor: self.colorSet.dataColor2)
    private lazy var dataIterator = LoopIterator<[LineChartData]>(collection: [self.altitudeData, self.temprData])
    
    /// Data that view subscribes on
    struct Output {
        /// Color scheme for view. Just replace implementation to change color scheme
        let appearance: Observable<ApplicationColorScheme>
        let navigationTitle: Observable<String>
        let chartData: Observable<LineChartData>
        /// Notify that new dweet was received
        let newDweet: Observable<Double>
    }
    
    /// Data from view
    struct Input {
        let switchData: Driver<Void>
    }
    
    func transform(input: Input) -> Output {
        let appearance = Observable<ApplicationColorScheme>.just(self.colorSet)
        let navigationTitle: Observable<String> = .just("Realtime Data Viewer")
        let data = Observable<LineChartData>.just(self.temprData).concat(
            input.switchData
                .map { [weak self] _ -> LineChartData? in
                    return self?.dataIterator.next()
                }
                .asObservable()
                .unwrap()
            )
        
        return Output(appearance: appearance,
                      navigationTitle: navigationTitle,
                      chartData: data,
                      newDweet: self.newDweet.asObservable()
        )
    }
    
    init(colorScheme: ApplicationColorScheme = MainAppColorScheme(), dataService: DweetService<Temperature> = TemperatureDweetService()) {
        self.dataService = dataService
        self.colorSet = colorScheme
        self.startFetching()
    }
    
    private func createData(label: String, lineColor: UIColor) -> LineChartData {
        let set: LineChartDataSet = LineChartDataSet(values: [ChartDataEntry](), label: label)
        set.colors = [lineColor]
        set.mode = .cubicBezier
        set.lineWidth = 2
        let data = LineChartData(dataSet: set)
        return data
    }
    
    private func startFetching() {
        let listener = self.dataService.listenDweets()
            .map { $0 as Dweet<Temperature>? }
            .catchErrorJustReturn(nil)
        
        let first5Dweets = self.dataService.last5Dweets()
            .map { $0 as [Dweet<Temperature>?] }
            .flatMap { Observable.from($0.reversed()) }
            .catchErrorJustReturn(nil)
        
        first5Dweets.asObservable()
            .concat(listener)
            .unwrap()
            .do(
                onNext: { [weak self] dweet in
                    guard let `self` = self else { return }
                    if let td = TemperatureChartDataEntry(dweet: dweet) as ChartDataEntry? {
                        self.temprData.addEntry(td, dataSetIndex: 0)
                    }
                    if let pd = PositionChartDataEntry(dweet: dweet) as ChartDataEntry? {
                        self.altitudeData.addEntry(pd, dataSetIndex: 0)
                    }
                }
            )
            .map { Date(isoString: $0.created)?.timeIntervalSince1970 }
            .unwrap()
            .subscribe(newDweet)
            .disposed(by: self.disposeBag)
    }
    
}
