//
//  DataChartViewController.swift
//  DweetBroadcast
//
//  Created by Nick Kibish on 3/3/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import UIKit
import DweetNetworkService
import Charts
import RxSwift

class DataChartViewController: UIViewController, Appearance {
    let disposeBag = DisposeBag()
    
    @IBOutlet var chartView: LineChartView!
    
    var viewModel: DataChartViewModel!
    weak var coordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bind(self.viewModel)
        self.chartView.xAxis.valueFormatter = DatesAxisFormatter()
        self.coordinator?.displayTutorialIfNeeded()
    }
    
    func bind(_ viewModel: DataChartViewModel) {
        let recognizer = UITapGestureRecognizer()
        recognizer.numberOfTapsRequired = 2
        self.chartView.addGestureRecognizer(recognizer)
        
        let input = DataChartViewModel.Input(switchData: recognizer.rx.event.asDriver().map {_ in () })
        let output = self.viewModel.transform(input: input)
        
        output.appearance
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(
                onNext: { [weak self] colorScheme in
                    self?.setColorScheme(colorScheme)
                }
            )
            .disposed(by: self.disposeBag)
        
        output.navigationTitle
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(
                onNext: { [weak self] title in
                    self?.navigationItem.title = title
                }
            )
            .disposed(by: self.disposeBag)
        
        output.chartData
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(
                onNext: { [weak self] chartData in
                    self?.chartView.data = chartData
                }
            )
            .disposed(by: self.disposeBag)
        
        output.newDweet
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(
                onNext: { [weak self] xPos in
                    self?.chartView.notifyDataSetChanged()
                    self?.chartView.moveViewToX(xPos)
                }
            )
            .disposed(by: self.disposeBag)
    }

}

