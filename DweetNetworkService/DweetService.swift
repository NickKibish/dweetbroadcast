//
//  DweetService.swift
//  DweetNetworkService
//
//  Created by Nick Kibish on 3/5/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import RxCocoa
import RxSwift

/// Class for fetching dweets from dweet.io
open class DweetService<T: Decodable> {
    private let network: Network<DweetResponse<T>> = Network()
    private let listener: Network<Dweet<T>> = Network()
    private let thing: String

    /// Init with thing
    ///
    /// - Parameter thing: thing name
    public init(thing: String) {
        self.thing = thing
    }
    
    /// Get last dweets
    ///
    /// - Returns: Observable for last dweets
    open func last5Dweets() -> Observable<[Dweet<T>]> {
        let path = "https://dweet.io:443/get/dweets/for/\(self.thing)"
        return self.network.getItem(path)
            .map {
                $0.with
        }
    }
    
    /// Listen to dweets
    ///
    /// - Returns: Observable for generic dweets
    open func listenDweets() -> Observable<Dweet<T>> {
        return self.listener.listen("https://dweet.io/listen/for/dweets/from/\(self.thing)")
    }
}

/// Implementation for fetching Temperature dweets
public final class TemperatureDweetService: DweetService<Temperature> {
    public init() {
        // Change name to get dweets from another thing
        super.init(thing: "momentous-sleep")
    }
}
