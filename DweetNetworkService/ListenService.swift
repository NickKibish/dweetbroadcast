//
//  ListenService.swift
//  DweetNetworkService
//
//  Created by Nick Kibish on 3/5/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import Foundation
import CFNetwork
import RxSwift

/// Class which opens connection and listen events from server
class ListenService: NSObject {
    private var stream: InputStream!
    /// Stream of data from server
    let observableData = PublishSubject<Data>()
    private let endpoint: String
    
    init(endpoint: String) {
        self.endpoint = endpoint
        super.init()
    }
    
    deinit {
        self.observableData.onCompleted()
    }
    
    /// Setup connection to the server
    func establish() {
        let urlString = self.endpoint as CFString
        let url = CFURLCreateWithString(kCFAllocatorDefault, urlString, nil)!
        let method = "GET" as CFString
        
        let request = CFHTTPMessageCreateRequest(kCFAllocatorDefault, method, url, kCFHTTPVersion1_1)
        let readStream = CFReadStreamCreateForHTTPRequest(kCFAllocatorDefault, request.takeRetainedValue())
        
        self.stream = readStream.takeRetainedValue() as InputStream
        self.stream.delegate = self
        self.stream.schedule(in: RunLoop.current, forMode: .common)
        self.stream.open()
        RunLoop.current.run()
    }
}

extension ListenService: StreamDelegate {
    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        let input = aStream as! InputStream
        switch eventCode {
        case .hasBytesAvailable:
            let bufferSize = 1024
            let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: bufferSize)
            while input.hasBytesAvailable {
                let read = input.read(buffer, maxLength: bufferSize)
                var data = Data()
                data.append(buffer, count: read)
                self.observableData.onNext(data)
            }
            buffer.deallocate()
        case .errorOccurred, .endEncountered:
            // Just reopen connection on error or if connection was closed
            input.close()
            self.establish()
        default:
            break
        }
    }
}
