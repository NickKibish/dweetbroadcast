//
//  Network.swift
//  DweetNetworkService
//
//  Created by Nick Kibish on 3/5/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import RxSwift
import RxCocoa

/// Abstract class for making requests
class Network<T: Decodable> {
    enum Error: Swift.Error {
        case badURL(String)
    }
    
    private let scheduler: ConcurrentDispatchQueueScheduler
    private var listenService: ListenService!
    
    public init() {
        self.scheduler = ConcurrentDispatchQueueScheduler(qos: DispatchQoS(qosClass: DispatchQoS.QoSClass.background, relativePriority: 1))
    }
    
    /// Get one item with provided Url
    ///
    /// - Parameter urlString: URL for request
    /// - Returns: Observable for item type
    public func getItem(_ urlString: String) -> Observable<T> {
        guard let url = URL(string: urlString) else {
            return Observable.error(Error.badURL(urlString))
        }
        
        let request = URLRequest(url: url)
        
        return URLSession(configuration: .default)
            .rx.data(request: request)
            .observeOn(self.scheduler)
            .map {
                return try JSONDecoder().decode(T.self, from: $0)
            }
    }
    
    /// Open connection and start listening to items
    ///
    /// - Parameter urlString: Request URL
    /// - Returns: Observable of items
    public func listen(_ urlString: String) -> Observable<T> {
        if case .none = URL(string: urlString) {
            return Observable.error(Error.badURL(urlString))
        }
        
        let listener = ListenService(endpoint: urlString)
        DispatchQueue.global().async {
            listener.establish()
        }
        
        return listener.observableData
            .flatMap { data -> Observable<T> in
                let dataString = String(data: data, encoding: .utf8)?
                    // Remove extra characters
                    .replacingOccurrences(of: "\\", with: "")
                    .replace(regex: "^[0-9]*\\s*\"", with: "")
                    .replace(regex: "\\s*\"$", with: "")
            
                if let jsonData = dataString?.data(using: .utf8),
                    let obj = try? JSONDecoder().decode(T.self, from: jsonData) {
                    return Observable.just(obj)
                }
                return .empty()
            }
    }
}
