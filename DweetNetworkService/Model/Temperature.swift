//
//  Temperature.swift
//  DweetNetworkService
//
//  Created by Nick Kibish on 3/5/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import Foundation

public struct Temperature: Codable {
    public let deviceName, deviceLocation: String
    public let temperatureData: Double
    public let temperatureUnit: String
    public let accelerometerData: AccelerometerData
    
    public struct AccelerometerData: Codable {
        public let x, y, z: Double
    }
    
    enum CodingKeys: String, CodingKey {
        case deviceName = "device_name"
        case deviceLocation = "device_location"
        case temperatureData = "temperature_data"
        case temperatureUnit = "temperature_unit"
        case accelerometerData = "accelerometer_data"
    }
}
