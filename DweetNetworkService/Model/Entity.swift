//
//  Entity.swift
//  DweetBroadcast
//
//  Created by Nick Kibish on 3/5/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import Foundation

public struct DweetResponse<T: Decodable>: Decodable {
    public let this, by, the: String
    public let with: [Dweet<T>]
}

public struct Dweet<T: Decodable>: Decodable {
    public let thing, created: String
    public let content: T
}
