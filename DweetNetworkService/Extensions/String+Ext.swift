//
//  String+Ext.swift
//  DweetNetworkService
//
//  Created by Nick Kibish on 3/6/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

import Foundation

extension String {
    func replace(regex: String, with template: String) -> String {
        guard let exp = try? NSRegularExpression(pattern: regex, options: .caseInsensitive) else { return self }
        return exp.stringByReplacingMatches(in: self, options: [], range: NSRange(location: 0, length: self.count), withTemplate: template)
    }
}
