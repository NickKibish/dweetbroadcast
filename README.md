# Dweet.io Broadcast
## App Overview
The app consists of one screen with data chart. 

Chart can display two types of data:
- Temperature (red line)
- Altitude (green line)

To switch between data types just double click on the screen.
Application loads last stored dweets and after that it starts listening to new data. Once new dweet is available it's appeared on the time line chart.

## Problem I faced with
Totally I spent 3 evenings to create the app.
However I spent almost day to figure out how to connect and start listening to data from server. Neither Alamofire nor URLSession itself returned me any data. Finally I gave up attempts to get data using URLSessionTasks and decided implement low level layer using CoreFoundation. 

Current implementation uses CFNetworking to establish connection and opens InputStream to read data from server. If it gets error or stream is closed for some reason, service just reopens connection.

Another problem is that thing you provided doesn't emit new data. So I've created my own thing with completely the same structure. I've also ran script which emits new random data each 10 seconds. If you wish to use your thing you should set its name in `DweetService.swift` line 48.

## Architecture
Through the app I use some kind of MVVM. 

For handling viewControllers I use `Coordinator`.

I also separated business logic and Application. All network services are located in the isolated framework. 